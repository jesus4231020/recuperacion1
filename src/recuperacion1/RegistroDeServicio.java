
package recuperacion1;

public class RegistroDeServicio {
    protected int numServicio;
    protected String fecha;
    protected int tipoServicio;
    protected Vehiculo vehiculo;
    protected String descripcion;
    protected int costoMantenimiento;
    protected int tipodemotor;

    //  Constructor
    public RegistroDeServicio(int numServicio, String fecha, int tipoServicio, Vehiculo vehiculo, String descripcion, int costoMantenimiento, int tipodemotor) {
        this.numServicio = numServicio;
        this.fecha = fecha;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descripcion = descripcion;
        this.costoMantenimiento = costoMantenimiento;
        this.tipodemotor = tipodemotor;
    }

    public RegistroDeServicio() {
        this.numServicio = 0;
        this.fecha = "";
        this.tipoServicio = 0;
        this.vehiculo = null;
        this.descripcion = "";
        this.costoMantenimiento = 0;
        this.tipodemotor = 0;
    }

    public RegistroDeServicio(RegistroDeServicio b) {
        this.numServicio = b.numServicio;
        this.fecha = b.fecha;
        this.tipoServicio = b.tipoServicio;
        this.vehiculo = b.vehiculo;
        this.descripcion = b.descripcion;
        this.costoMantenimiento = b.costoMantenimiento;
        this.tipodemotor = b.tipodemotor;
    }

    // Getters y setters
    public int getNumServicio() {
        return numServicio;
    }

    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCostoMantenimiento() {
        return costoMantenimiento;
    }

    public void setCostoMantenimiento(int costoMantenimiento) {
        this.costoMantenimiento = costoMantenimiento;
    }

    public int getTipodemotor() {
        return tipodemotor;
    }

    public void setTipodemotor(int tipodemotor) {
        this.tipodemotor = tipodemotor;
    }

    // Método para calcular el costo de servicio basado en el tipo de motor
    public void calcularCostoServicio() {
        double incremento = 0;

        switch (tipodemotor) {
            case 1: // 4 cilindros
                incremento = 0.25;
                break;
            case 2: // 6 cilindros
                incremento = 0.50;
                break;
            case 3: // 8 cilindros
                incremento = 1.50;
                break;
            case 4: // 10 cilindros
                incremento = 2.00;
                break;
            default:
                incremento = 0;
                break;
        }

        this.costoMantenimiento += this.costoMantenimiento * incremento;
    }
}