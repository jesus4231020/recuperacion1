
package recuperacion1;


public abstract class Vehiculo {
    
    protected int numserie;
    protected int tipo;
    protected String marca;
    protected String modelo;
    
    // constructores 

    public Vehiculo(int numserie, int tipo, String marca, String modelo) {
        this.numserie = numserie;
        this.tipo = tipo;
        this.marca = marca;
        this.modelo = modelo;
    }
     public Vehiculo() {
        this.numserie = 0;
        this.tipo = 0;
        this.marca = "";
        this.modelo = "";
    }
     public Vehiculo(Vehiculo v) {
        this.numserie = v.numserie;
        this.tipo = v.tipo;
        this.marca = v.marca;
        this.modelo = v.modelo;
    }

    public int getNumserie() {
        return numserie;
    }

    public void setNumserie(int numserie) {
        this.numserie = numserie;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
     
    
}
