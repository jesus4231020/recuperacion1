
package recuperacion1;


public class VehiculoCarga extends Vehiculo {
    protected int toneladas;

    public VehiculoCarga(int toneladas) {
        this.toneladas = toneladas;
    }
    public VehiculoCarga() {
        this.toneladas = 0;
    }
    public VehiculoCarga(VehiculoCarga r) {
        this.toneladas = r.toneladas;
    }

    public int getToneladas() {
        return toneladas;
    }

    public void setToneladas(int toneladas) {
        this.toneladas = toneladas;
    }
    
}
